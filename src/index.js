import app from "./app";

const logger = app.get("logger");

process.on("unhandledRejection", (reason, p) =>
  logger.error("Unhandled Rejection at: Promise ", p, reason),
);

app.get("mongooseClient").connect(app.config.get("mongo"));

app.listen(app.config.get("port"), () => {
  logger.info(`listening on port ${app.config.get("port")}`);
});
