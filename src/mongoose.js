import mongoose from "mongoose";

export default app => {
  mongoose.Promise = global.Promise;

  app.set("mongooseClient", mongoose);
};
