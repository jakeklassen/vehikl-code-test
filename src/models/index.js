import registerTicketModel from "./ticket";

export default app => {
  registerTicketModel(app);
};
