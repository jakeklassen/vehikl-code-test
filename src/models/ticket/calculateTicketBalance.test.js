/**
 * @jest-environment node
 */

import subHours from "date-fns/sub_hours";
import calculateTicketBalance, { Costs } from "./calculateTicketBalance";

test("should throw if `createdAt` is not a valid date value", () => {
  expect(() => calculateTicketBalance()).toThrow();
  expect(() => calculateTicketBalance({ createdAt: "test" })).toThrow();
  expect(() =>
    calculateTicketBalance({ createdAt: new Date("1999-02-02BAD12:12:12") }),
  ).toThrow();
});

test(`should return ${Costs.LessThanAnHour} when duration is <= 1 hour`, () => {
  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 1) }),
  ).toEqual(Costs.LessThanAnHour);

  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 1.1) }),
  ).not.toEqual(Costs.LessThanAnHour);
});

test(`should return ${
  Costs.BetweenOneAndThreeHours
} when duration is > 1 hour and <= 3 hours`, () => {
  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 2) }),
  ).toEqual(Costs.BetweenOneAndThreeHours);

  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 3.1) }),
  ).not.toEqual(Costs.BetweenOneAndThreeHours);
});

test(`should return ${
  Costs.BetweenThreeAndSixHours
} when duration is > 3 hour and <= 6 hours`, () => {
  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 4) }),
  ).toEqual(Costs.BetweenThreeAndSixHours);

  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 6.1) }),
  ).not.toEqual(Costs.BetweenThreeAndSixHours);
});

test(`should return ${
  Costs.MoreThanSixHours
} when duration is > 6 hours`, () => {
  expect(
    calculateTicketBalance({ createdAt: subHours(new Date(), 6.1) }),
  ).toEqual(Costs.MoreThanSixHours);
});
