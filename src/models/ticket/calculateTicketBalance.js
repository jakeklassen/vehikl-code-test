import differenceInMinutes from "date-fns/difference_in_minutes";
import isValid from "date-fns/is_valid";

const BaseDollarRate = 3;

export const Costs = {
  LessThanAnHour: BaseDollarRate,
  BetweenOneAndThreeHours: BaseDollarRate * 1.5,
  BetweenThreeAndSixHours: BaseDollarRate * 2,
  MoreThanSixHours: BaseDollarRate * 2.5,
};

export const MinuteIntervals = {
  OneHour: 60,
  ThreeHours: 180,
  SixHours: 360,
};

/**
 * Given a ticket object, determine the total cost due
 * based on the ticket creation time and now.
 * @param {Object} ticket Ticket object
 */
const calculateTicketBalance = (ticket = {}) => {
  if (!isValid(ticket.createdAt)) {
    throw new Error("Ticket must provide a valid `createdAt` date.");
  }

  const minutes = differenceInMinutes(new Date(), ticket.createdAt);
  let total = Costs.LessThanAnHour;

  if (minutes > MinuteIntervals.SixHours) {
    total = Costs.MoreThanSixHours;
  } else if (minutes > MinuteIntervals.ThreeHours) {
    total = Costs.BetweenThreeAndSixHours;
  } else if (minutes > MinuteIntervals.OneHour) {
    total = Costs.BetweenOneAndThreeHours;
  }

  return total;
};

export default calculateTicketBalance;
