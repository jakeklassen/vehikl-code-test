export const Status = {
  Paid: "paid",
  Unpaid: "unpaid",
};

const register = app => {
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;

  const TicketSchema = new Schema(
    {
      status: {
        type: String,
        enum: Object.values(Status),
        default: Status.Unpaid,
        index: true,
      },
    },
    {
      timestamps: true,
    },
  );

  const Ticket = mongooseClient.model("Ticket", TicketSchema);
  app.models.set("Ticket", Ticket);

  return Ticket;
};

export default register;
