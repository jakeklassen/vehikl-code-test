import post from "./post";
import get from "./get";

export default app => ({
  create: post(app),
  get: get(),
});
