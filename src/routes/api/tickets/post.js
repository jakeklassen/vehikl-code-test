export default app => async (req, res, next) => {
  try {
    const Ticket = app.models.get("Ticket");

    const doc = await Ticket.create({});

    res.status(201).json(doc);
  } catch (error) {
    next(error);
  }
};
