import calculateTicketBalance from "../../../models/ticket/calculateTicketBalance";

export default () => (req, res, next) => {
  try {
    res.json({
      ...req.ticket,
      balance: calculateTicketBalance(req.ticket),
    });
  } catch (error) {
    next(error);
  }
};
