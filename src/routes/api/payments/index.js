import post from "./post";

export default app => ({
  create: post(app),
});
