import { BadRequest } from "@feathersjs/errors";
import cardValidator from "card-validator";
import { Status } from "../../../models/ticket";

export default app => async (req, res, next) => {
  try {
    const Ticket = app.models.get("Ticket");
    const { creditCard } = req.body || {};

    if (!cardValidator.number(creditCard).isPotentiallyValid) {
      throw new BadRequest("Invalid credit card supplied", { creditCard });
    }

    await Ticket.updateOne({ _id: req.ticket._id }, { status: Status.Paid });

    res.end();
  } catch (error) {
    next(error);
  }
};
