import request from "supertest";
import { Types } from "mongoose";
import { Status as TicketStatus } from "../../models/ticket";
import { TicketAlreadyPaid } from "./errorCodes";

const app = global.app;

describe("'payments' api", () => {
  beforeEach(async () => app.models.get("Ticket").remove());

  test("should respond with 404 if ticket cannot be found", async () => {
    await request(app)
      .post(`/api/payments/${Types.ObjectId()}`)
      .expect(404);
  });

  test("should respond with 400 if invalid credit card is supplied", async () => {
    const ticket = await app.models.get("Ticket").create({});

    await request(app)
      .post(`/api/payments/${ticket.id}`)
      .send({
        creditCard: "fail",
      })
      .expect(400);
  });

  test("should respond with 403 if ticket is already paid", async () => {
    const ticket = await app.models.get("Ticket").create({
      status: TicketStatus.Paid,
    });

    await request(app)
      .post(`/api/payments/${ticket.id}`)
      .send({
        creditCard: "4111111111111111",
      })
      .expect(403)
      .then(res => {
        expect(res.body.data.code).toEqual(TicketAlreadyPaid);
      });
  });

  test("should respond with 200 and mark ticket as paid", async () => {
    const Ticket = app.models.get("Ticket");
    const ticket = await Ticket.create({});

    await request(app)
      .post(`/api/payments/${ticket.id}`)
      .send({
        creditCard: "4111111111111111",
      })
      .expect(200);

    const latestTicket = await Ticket.findById(ticket.id);
    expect(latestTicket.status).toEqual(TicketStatus.Paid);
  });
});
