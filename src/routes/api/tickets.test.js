import request from "supertest";
import { Types } from "mongoose";
import { Status as TicketStatus } from "../../models/ticket";
import { NoSpotsAvailable } from "./errorCodes";

const app = global.app;

describe("'tickets' api", () => {
  beforeEach(async () => app.models.get("Ticket").remove());

  test("should respond with 404 if ticket cannot be found", async () => {
    await request(app)
      .get(`/api/tickets/${Types.ObjectId()}`)
      .expect(404);
  });

  test("should create new ticket when spots are available", async () => {
    await request(app)
      .post("/api/tickets")
      .expect(201)
      .then(res => {
        expect(res.body._id).toBeDefined();
        expect(res.body.status).toEqual(TicketStatus.Unpaid);
      });
  });

  test("should respond with ticket containing balance if found", async () => {
    const ticket = await app.models.get("Ticket").create({});

    await request(app)
      .get(`/api/tickets/${ticket.id}`)
      .expect(200)
      .then(res => {
        expect(parseFloat(res.body.balance)).not.toBeNaN();
      });
  });

  test("should respond with 403 if no spots are available", async () => {
    await Promise.all(
      Array.from({ length: app.config.get("maxParkingSpots") }, () =>
        app.models.get("Ticket").create({}),
      ),
    );

    await request(app)
      .post("/api/tickets")
      .expect(403)
      .then(res => {
        expect(res.body.data.code).toEqual(NoSpotsAvailable);
      });
  });
});
