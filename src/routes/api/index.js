import tickets from "./tickets";
import payments from "./payments";

export default app => ({
  tickets: tickets(app),
  payments: payments(app),
});
