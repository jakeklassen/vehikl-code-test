import initApi from "./api";
import loadTicket from "../middleware/loadTicket";
import requireOpenSpace from "../middleware/requireOpenSpace";
import requireUnpaidTicket from "../middleware/requireUnpaidTicket";

export default app => {
  const api = initApi(app);

  app.get("/api/tickets/:id", loadTicket(app), api.tickets.get);
  app.post("/api/tickets", requireOpenSpace(app), api.tickets.create);

  app.post(
    "/api/payments/:ticketId",
    loadTicket(app),
    requireUnpaidTicket(),
    api.payments.create,
  );
};
