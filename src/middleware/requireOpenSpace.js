import { Forbidden } from "@feathersjs/errors";
import { Status } from "../models/ticket";
import { NoSpotsAvailable } from "../routes/api/errorCodes";

export default app => async (req, res, next) => {
  try {
    const Ticket = app.models.get("Ticket");
    const maxParkingSpots = app.config.get("maxParkingSpots");

    const currentUnpaidTickets = await Ticket.count({
      status: Status.Unpaid,
    });

    if (currentUnpaidTickets >= maxParkingSpots) {
      next(new Forbidden("No more spaces", { code: NoSpotsAvailable }));
    } else {
      next();
    }
  } catch (error) {
    next(error);
  }
};
