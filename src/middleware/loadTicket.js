import { NotFound } from "@feathersjs/errors";

export default app => async (req, res, next) => {
  try {
    const Ticket = app.models.get("Ticket");
    const id = req.params.id || req.params.ticketId;

    const doc = await Ticket.findById(id).lean();

    if (!doc) {
      throw new NotFound("Ticket not found");
    }

    req.ticket = doc;

    next();
  } catch (error) {
    next(error);
  }
};
