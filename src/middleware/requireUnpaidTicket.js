import { Forbidden } from "@feathersjs/errors";
import { Status } from "../models/ticket";
import { TicketAlreadyPaid } from "../routes/api/errorCodes";

export default () => async (req, res, next) => {
  try {
    if (req.ticket.status === Status.Paid) {
      throw new Forbidden("Ticket is already paid", {
        code: TicketAlreadyPaid,
      });
    }

    next();
  } catch (error) {
    next(error);
  }
};
