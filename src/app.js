import express from "express";
import { notFound, errorHandler } from "@feathersjs/express";
import helmet from "helmet";
import logger from "winston";
import config from "config";
import bodyParser from "body-parser";
import initMongoose from "./mongoose";
import registerModels from "./models";
import routes from "./routes";

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set("config", config);
app.set("logger", logger);

app.config = config;
app.models = new Map();

logger.level = app.config.get("logLevel");

app.use(helmet());

initMongoose(app);
registerModels(app);

routes(app);

app.use(notFound());
app.use(errorHandler({ logger }));

export default app;
