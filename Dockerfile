FROM node:8.11.3-alpine

WORKDIR /home/node/app

# Install app dependencies
COPY package.json .
RUN yarn
