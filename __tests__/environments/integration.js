/**
 * Custom node test environment used for integration tests.
 * It will create unique databases and drop them afterwards.
 */

import NodeEnvironment from "jest-environment-node";
import shortid from "shortid";
import app from "../../src/app";

export default class Integration extends NodeEnvironment {
  async setup() {
    await super.setup();

    const dbName = `parking_app_${shortid()}`;
    const connectionUri = `${app.config.get("mongo")}/${dbName}`;

    await app.get("mongooseClient").connect(
      connectionUri,
      { config: { autoIndex: false } },
    );

    this.global.app = app;
  }

  async teardown() {
    await app.get("mongooseClient").connection.db.dropDatabase();
    await app.get("mongooseClient").disconnect();

    await super.teardown();
  }

  runScript(script) {
    return super.runScript(script);
  }
}
