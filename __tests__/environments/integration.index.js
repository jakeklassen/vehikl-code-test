// Jest does not seem to play well with ES Modules when creating
// custom environments, so I use esm to load my module.

// eslint-disable-next-line no-global-assign
require = require("esm")(module);

module.exports = require("./integration").default;
