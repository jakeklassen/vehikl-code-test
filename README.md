# Vehikl Code Test

## How to Run

The assumption is that `docker-compose` is available:

```shell
$ docker-compose up # spin up services
```

This will install dependencies if `node_modules` cannot be found.

**Node:** I am using 8.11.x, but if you have nvm installed, just run `nvm use` to adhere to my `.nvmrc`.

The api is running at `http://localhost:3000/api`.

The mongo database can be accessed at `mongodb://localhost:27017` outside of docker, or at `mongodb://mongo` from within services.

Changes are reloaded via `nodemon`.

## Tests

Run `yarn test` to run all tests.

Tests are run in parallel using per file databases for integration tests. A custom Jest environment will create and drop these databases so don't worry about cleanup.

# Deployment

I would deploy using [now](https://zeit.co/now) - via GitLab CI or locally, because I have experience with it, but won't demonstrate that here. I don't want to extend my paid account further. The database would be on [Atlas](https://www.mongodb.com/cloud/atlas). Both of these options keep management at an absolute minimum.

# Implementation Notes

## Ticket Number

I did not use ticket numbers as the identifier anywhere in the API. MongoDB does not support auto incrementing numberic ids - without extra work, and the number itself seems to be of little value except for some possible display information on a ticket stub.

I used the `ObjectId` native to MongoDB.

## `GET /tickets/:ticketId`

I interpreted this to not return a number literal in the response. I instead added a computed property - `balance`, to the response object.

# Feedback

## Time Allotted

The one week timeline is nice, and respectful of the applicants time.

## Ticket Number as Identifier

The `ticket#` as the identifier for tickets in the API doesn't make a whole lot of sense. It's a bit ambiguous in this scenario. `ticketId` would make more sense and be more logically interpreted by any applicant.

It's also not a great choice with many applicants from various stacks. Example: MongoDB - no native auto incrementing id, and a popular NoSQL database option.

## `GET /tickets/{ticket#}` Returns Number

Whether or not you intended `GET /tickets/{ticket#}` to return a numeric literal aside, it's easily open to interpretation. Let's say I do go with the numeric literal approach, I'm now forced to query tickets via a model or some other abstraction. This is especially painful if I was hoping to create services on top of my own local APIs.

I actually abandoned doing this entire project in [Feathers](https://feathersjs.com/) because it would have been more effort to work _around_ the repercussions of this. I instead wrote it in Express, which somewhat turned me off of the whole project if I'm honest. Not because I don't enjoy Express, but because simple changes would make this a non-issue.

## `POST /tickets/{ticket#}`

This endpoint stood out to me. Mostly because it doesn't read like most conventions without consulting documentation. Why the distinction for `ticket#` in the url at all? Or instead require it as a body parameter along with the credit card. At least this way I could interpret the endpoint better, even if I was a bit puzzled that no new payment record is created on response.

Since there is no mention of storing payments, something like `POST /tickets/{ticket#}/pay` would be nice. If you hate nested actions, my previous suggestion doesn't suffer from this.
